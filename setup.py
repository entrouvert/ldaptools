#! /usr/bin/env python

import os
import subprocess

from setuptools import setup
from setuptools.command.sdist import sdist


class eo_sdist(sdist):
    def run(self):
        print('creating VERSION file')
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print('removing VERSION file')
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result.replace('.dirty', '+dirty')
            return version
        else:
            return '0.0.post%s' % len(
                subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines()
            )
    return '0.0'


setup(
    name='ldaptools',
    version=get_version(),
    license='AGPLv3+',
    description='ldaptools',
    long_description=open('README.rst').read(),
    url='http://dev.entrouvert.org/projects/ldaptools/',
    author="Entr'ouvert",
    author_email='authentic@listes.entrouvert.com',
    maintainer='Benjamin Dauvergne',
    maintainer_email='bdauvergne@entrouvert.com',
    packages=['ldaptools'],
    package_dir={'': 'src'},
    package_data={'ldaptools': ['schemas/*.ldif']},
    install_requires=['python-ldap>2', 'six'],
    entry_points={
        'console_scripts': ['ldapsync=ldaptools.ldapsync.cmd:main'],
    },
    zip_safe=False,
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Topic :: System :: Systems Administration :: Authentication/Directory',
        'Programming Language :: Python',
    ],
    cmdclass={'sdist': eo_sdist},
)
